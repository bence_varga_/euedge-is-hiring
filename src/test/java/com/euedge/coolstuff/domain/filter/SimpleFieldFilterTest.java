package com.euedge.coolstuff.domain.filter;

import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.exception.ValidationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.Assert;

import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class )
public class SimpleFieldFilterTest {

    private SimpleFieldFilter simpleFieldFilter;

    @Before
    public void setUp() throws Exception {
        simpleFieldFilter = new SimpleFieldFilter();

    }

    @Test
    public void testRuleListIsEmptyThenReturnTrue() throws Exception {
        Assert.isTrue(simpleFieldFilter.apply(new Product()));


    }

    @Test
    public void testRuleContainsValidCriteriaForStringTypeFieldWhichFiltersProductThenReturnFalse() throws Exception {
        final String nameFieldValue = "valami";
        simpleFieldFilter.setRule(Arrays.asList(new Criteria("name", Condition.EQUAL, nameFieldValue)));

        final Product product = new Product();
        product.setName(nameFieldValue);
        Assert.isTrue(!simpleFieldFilter.apply(product));
    }

    @Test(expected = ValidationException.class)
    public void testRuleContainsInValidCriteriaForStringTypeThenThrowException() throws Exception {
        final String nameFieldValue = "valami";
        simpleFieldFilter.setRule(Arrays.asList(new Criteria("name", Condition.GREATER_THEN, nameFieldValue)));

        final Product product = new Product();
        product.setName(nameFieldValue);
        simpleFieldFilter.apply(product);
    }


    @Test
    public void testRuleContainsValidCriteriaStringTypeFieldWhichNotFiltersProductThenReturnTrue() throws Exception {
        final String nameFieldValue = "valami";
        simpleFieldFilter.setRule(Arrays.asList(new Criteria("name", Condition.EQUAL, nameFieldValue + "1")));

        final Product product = new Product();
        product.setName(nameFieldValue);
        Assert.isTrue(simpleFieldFilter.apply(product));
    }

    @Test
    public void testRuleContainsValidCriteriaForDoubleTypeFieldThenProductFilteredAndReturnFalse() throws Exception {
        simpleFieldFilter.setRule(Arrays.asList(new Criteria("price", Condition.EQUAL, "10.0")));
        final Product product = new Product();
        product.setPrice(10.0);
        Assert.isTrue(!simpleFieldFilter.apply(product));
    }

    @Test(expected = ValidationException.class)
    public void testRuleContainsInValidCriteriaForDoubleTypeFieldThenThrowException() throws Exception {
        simpleFieldFilter.setRule(Arrays.asList(new Criteria("price", Condition.EQUAL, "dsfsdf")));
        final Product product = new Product();
        product.setPrice(10.0);
        simpleFieldFilter.apply(product);
    }

    //TODO implement number specific tests


    @Test
    public void testRuleContainsGTCriteriaForNumberFieldThenReturnFalse() throws Exception {
        simpleFieldFilter.setRule(Arrays.asList(new Criteria("price", Condition.GREATER_THEN, "9")));
        final Product product = new Product();
        product.setPrice(10.0);
        Assert.isTrue(!simpleFieldFilter.apply(product));
    }

    @Test
    public void testRuleContainsGTCriteriaForNumberFieldButRecordFieldIsLessThenReturnTrue() throws Exception {
        simpleFieldFilter.setRule(Arrays.asList(new Criteria("price", Condition.GREATER_THEN, "11")));
        final Product product = new Product();
        product.setPrice(10.0);
        Assert.isTrue(simpleFieldFilter.apply(product));
    }

    @Test
    public void testRuleContainsLTCriteriaForNumberFieldThenReturnFalse() throws Exception {
        simpleFieldFilter.setRule(Arrays.asList(new Criteria("price", Condition.LESS_THEN, "11")));
        final Product product = new Product();
        product.setPrice(10.0);
        Assert.isTrue(!simpleFieldFilter.apply(product));
    }

    @Test
    public void testRuleContainsLTCriteriaForNumberFieldButRecordFieldIsLessThenReturnTrue() throws Exception {
        simpleFieldFilter.setRule(Arrays.asList(new Criteria("price", Condition.LESS_THEN, "9")));
        final Product product = new Product();
        product.setPrice(10.0);
        Assert.isTrue(simpleFieldFilter.apply(product));
    }
}