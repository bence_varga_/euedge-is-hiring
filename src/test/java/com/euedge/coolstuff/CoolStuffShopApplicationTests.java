package com.euedge.coolstuff;

import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.ProductScore;
import com.euedge.coolstuff.domain.ProductTag;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.port;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


@RunWith(SpringJUnit4ClassRunner.class)
@WebIntegrationTest("server.port:12345")
@SpringApplicationConfiguration(classes = CoolStuffShopApplication.class)
public class CoolStuffShopApplicationTests {

    // TODO: c reate unit tests for main functionality
    @Value("${server.port}")
    private int serverPort;

    @Before
    public void setUp() throws Exception {
        port = serverPort;

    }

    @Test
    public void testFilterEndpointIsCalledWithValidBodyAndHeaderContentThenReturn200() {
        given()
                .header("Content-Type", "application/json")
                .and()
                .body("[{ \"filter\":\"simpleFilter\",\"rule\": [{\"field\":\"price\",\"condition\":\"<\",\"value\":\"2\"}]}]}]")
                .when()
                .post("api/product/filter")
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void testFilterEndpointIsCalledWithoutBodyContentThenReturn400() {
        given()
                .header("Content-Type", "application/json")
                .and()
                .when()
                .post("api/product/filter")
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void testFilterEndpointIsCalledWithLessFilterOnPriceFieldThenReturnAppropriateElements() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        String response = given()
                .header("Content-Type", "application/json")
                .and()
                .body("[{ \"filter\":\"simpleFilter\",\"rule\": [{\"field\":\"price\",\"condition\":\"<\",\"value\":\"9\"}]}]}]")
                .when()
                .post("api/product/filter").asString();

        List<Product> result = mapper.readValue(response, new TypeReference<List<Product>>() {
        });

        assertTrue(result.stream().filter(product -> product.getPrice() < 9).count() == 0);

    }

    @Test
    public void testFilterEndpointIsCalledWithLessThenEqualFilterOnPriceFieldThenReturnAppropriateElements() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        String response = given()
                .header("Content-Type", "application/json")
                .and()
                .body("[{ \"filter\":\"simpleFilter\",\"rule\": [{\"field\":\"price\",\"condition\":\"<=\",\"value\":\"3\"}]}]}]")
                .when()
                .post("api/product/filter").asString();

        List<Product> result = mapper.readValue(response, new TypeReference<List<Product>>() {
        });
        assertTrue(result.stream().filter(product -> product.getPrice() <= 3).count() == 0);

    }

    @Test
    public void testFilterEndpointIsCalledWithGreaterFilterOnPriceFieldThenReturnAppropriateElements() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        String response = given()
                .header("Content-Type", "application/json")
                .and()
                .body("[{ \"filter\":\"simpleFilter\",\"rule\": [{\"field\":\"price\",\"condition\":\">\",\"value\":\"9\"}]}]}]")
                .when()
                .post("api/product/filter").asString();

        List<Product> result = mapper.readValue(response, new TypeReference<List<Product>>() {
        });

        assertTrue(result.stream().filter(product -> product.getPrice() > 9).count() == 0);

    }

    @Test
    public void testFilterEndpointIsCalledWithGreaterThenEqualFilterOnPriceFieldThenReturnAppropriateElements() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        String response = given()
                .header("Content-Type", "application/json")
                .and()
                .body("[{ \"filter\":\"simpleFilter\",\"rule\": [{\"field\":\"price\",\"condition\":\">=\",\"value\":\"3\"}]}]}]")
                .when()
                .post("api/product/filter").asString();

        List<Product> result = mapper.readValue(response, new TypeReference<List<Product>>() {
        });
        assertTrue(result.stream().filter(product -> product.getPrice() >= 3).count() == 0);

    }

    @Test
    public void testFilterEndpointIsCalledWithEqualFilterOnPriceFieldThenReturnAppropriateElements() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        String response = given()
                .header("Content-Type", "application/json")
                .and()
                .body("[{ \"filter\":\"simpleFilter\",\"rule\": [{\"field\":\"price\",\"condition\":\"=\",\"value\":\"10\"}]}]")
                .when()
                .post("api/product/filter").asString();

        List<Product> result = mapper.readValue(response, new TypeReference<List<Product>>() {
        });
        assertTrue(result.stream().filter(product -> product.getPrice() == 9).count() == 0);

    }

    @Test
    public void testFilterEndpointIsCalledWithScoresFieldFiltersThenReturnAppropriateElements() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        String response = given()
                .header("Content-Type", "application/json")
                .and()
                .body("[{ \"filter\":\"scoresFilter\",\"rule\":[{\"field\":\"score\",\"condition\":\">=\",\"value\":\"4\"}]}]")
                .when()
                .post("api/product/filter").asString();

        List<Product> result = mapper.readValue(response, new TypeReference<List<Product>>() {
        });
        for (Product product : result) {
            for (ProductScore score : product.getScores()) {
                if (score.getScore() >= 4.0) {
                    fail();
                }
            }
        }
    }

    @Test
    public void testFilterEndpointIsCalledWithScoresFieldFiltersWithEmptyThenReturnNotEmptyScores() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        String response = given()
                .header("Content-Type", "application/json")
                .and()
                .body("[{ \"filter\":\"scoresFilter\",\"rule\":[{\"empty\":false}]}]")
                .when()
                .post("api/product/filter").asString();

        List<Product> result = mapper.readValue(response, new TypeReference<List<Product>>() {
        });
        for (Product product : result) {
            if (product.getScores().isEmpty()) {
                fail();
            }
        }
    }

    @Test
    public void testFilterEndpointIsCalledWithTagsFieldFiltersThenReturnAppropriateProductsWithTags() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        String response = given()
                .header("Content-Type", "application/json")
                .and()
                .body("[{ \"filter\":\"tagsFilter\",\"rule\":[{\"value\":\"chicken\"}]}]")
                .when()
                .post("api/product/filter").asString();

        List<Product> result = mapper.readValue(response, new TypeReference<List<Product>>() {
        });
        for (Product product : result) {
            for (ProductTag tag : product.getProductTag()) {
                if ("chicken".equals(tag.getName())) {
                    fail();
                }
            }
        }
    }

    @Test
    public void testFilterEndpointIsCalledWithMultipleFieldFiltersThenReturnAppropriateProducts() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        String response = given()
                .header("Content-Type", "application/json")
                .and()
                .body("[{ \"filter\":\"tagsFilter\",\"rule\":[{\"value\":\"chicken\"}]}]")
                .when()
                .post("api/product/filter").asString();

        List<Product> result = mapper.readValue(response, new TypeReference<List<Product>>() {
        });
        for (Product product : result) {
            for (ProductTag tag : product.getProductTag()) {
                if ("chicken".equals(tag.getName())) {
                    fail();
                }
            }
        }
    }
}
