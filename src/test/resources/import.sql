--  PRODUCTS
insert into product(id,name,description,price,kosher)values(1,'Cool Melon','Green outside Red inside',10,true);
insert into product(id,name,description,price)values(2,'Coleslaw','Cool salads',3);
insert into product(id,name,description,price)values(3,'Chicken Wings','Fly away with the taste',7);
insert into product(id,name,description,price)values(4,'Beef Jerky','Moo',70);
insert into product(id,name,description,price,kosher)values(5,'Sweet Rolls','Green outside Red inside',3,true);
insert into product(id,name,description,price)values(6,'Coleslaw','Cool salads',44);
insert into product(id,name,description,price)values(7,'Chicken Wings','Fly away with the taste',12);
insert into product(id,name,description,price)values(8,'Beef Jerky','Moo',32);
-- AVAILABILITY
insert into PRODUCT_AVAILABILITY_INFO(PRODUCT_ID,AVAILABLE_ITEMS_COUNT)values(1,361);
insert into PRODUCT_AVAILABILITY_INFO(PRODUCT_ID,AVAILABLE_ITEMS_COUNT)values(2,0);
insert into PRODUCT_AVAILABILITY_INFO(PRODUCT_ID,AVAILABLE_ITEMS_COUNT)values(3,42);
insert into PRODUCT_AVAILABILITY_INFO(PRODUCT_ID,AVAILABLE_ITEMS_COUNT)values(4,6);
-- PRODUCT TAGS
insert into product_tag(id,name)values(1,'fruit');
insert into product_tag(id,name)values(2,'meat');
insert into product_tag(id,name)values(3,'chicken');
insert into product_tag(id,name)values(4,'vegetables');
insert into product_tag(id,name)values(5,'beef');
-- PRODUCT TAG LINKS
insert into product_tag_product(product_tag_id,product_id)values(1,1);
insert into product_tag_product(product_tag_id,product_id)values(2,3);
insert into product_tag_product(product_tag_id,product_id)values(3,3);
insert into product_tag_product(product_tag_id,product_id)values(4,2);
insert into product_tag_product(product_tag_id,product_id)values(2,4);
insert into product_tag_product(product_tag_id,product_id)values(5,4);
-- PRODUCT SCORES
insert into product_score(id,score,comment)values(1,3.5,'Not soo bad');
insert into product_score(id,score,comment)values(2,3, 'Good start, but...');
insert into product_score(id,score,comment)values(3,4, 'Good');
insert into product_score(id,score,comment)values(4,5, 'Epic');
insert into product_score(id,score,comment)values(5,0.5, 'PITA');
-- PRODUCT SCORE LINKS
insert into product_scores(product_id,scores_id)values(1,1);
insert into product_scores(product_id,scores_id)values(1,2);
insert into product_scores(product_id,scores_id)values(2,3);
insert into product_scores(product_id,scores_id)values(2,4);
insert into product_scores(product_id,scores_id)values(3,5);