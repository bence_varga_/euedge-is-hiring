package com.euedge.coolstuff.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Product {

	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private String description;
	private Double price;
	@JsonIgnore
	@OneToOne(mappedBy = "product", fetch = FetchType.EAGER)
	private ProductAvailabilityInfo availabilityInfo;
	@Column(columnDefinition = "boolean not null default false")
	private boolean kosher;
	@ManyToMany(mappedBy = "product", fetch = FetchType.EAGER)
	private List<ProductTag> productTag;
	@OneToMany
	private List<ProductScore> scores;
	@Transient
	private Double calculatedScore;

	public Product() {
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public ProductAvailabilityInfo getAvailabilityInfo() {
		return availabilityInfo;
	}

	public void setAvailabilityInfo(ProductAvailabilityInfo availabilityInfo) {
		this.availabilityInfo = availabilityInfo;
	}

	public boolean isKosher() {
		return kosher;
	}

	public void setKosher(boolean kosher) {
		this.kosher = kosher;
	}

	public List<ProductTag> getProductTag() {
		return productTag;
	}

	public void setProductTag(List<ProductTag> productTag) {
		this.productTag = productTag;
	}

	public List<ProductScore> getScores() {
		return scores;
	}

	public void setScores(List<ProductScore> scores) {
		this.scores = scores;
	}

	public Double getCalculatedScore() {
		return calculatedScore;
	}

	@PostLoad
	private void onLoad() {
		Double calculatedScore = 0d;
		if (scores != null && !scores.isEmpty()) {
			for (ProductScore score : scores) {
				calculatedScore += score.getScore();
			}
			calculatedScore /= scores.size();
		}
		this.calculatedScore = calculatedScore;
	}
}
