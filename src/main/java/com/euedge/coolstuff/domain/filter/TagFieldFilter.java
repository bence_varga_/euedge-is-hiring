package com.euedge.coolstuff.domain.filter;

import com.euedge.coolstuff.domain.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements logic for the {@link com.euedge.coolstuff.domain.ProductTag} type field .
 */
public class TagFieldFilter extends AbstractFieldSpecificFilter {

    private static Logger logger = LoggerFactory.getLogger(TagFieldFilter.class);
    @Override
    protected boolean isProductFilteredByCriteria(Product product, Criteria criteria) {
        logger.debug("{}.isProductFilteredByCriteria has been called with parameters: {} , {}", new Object[]{this.getClass().getSimpleName(),product,criteria});
        boolean isEmptyTagListNotAllowed = (null == product.getProductTag() || product.getProductTag().isEmpty()) && !criteria.getEmpty();
        boolean hasTag = product.getProductTag().stream().anyMatch(productTag -> productTag.getName().equals(criteria.getValue()));

        return  isEmptyTagListNotAllowed || hasTag;
    }
}
