package com.euedge.coolstuff.domain.filter;

import com.euedge.coolstuff.domain.Product;

public class DummyFilter implements ProductFilter {

    @Override
    public boolean apply(Product product) {
        return true;
    }

}
