package com.euedge.coolstuff.domain.filter;

import com.euedge.coolstuff.domain.exception.ValidationException;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by bence on 25/03/16 .
 */
public enum Condition {

    GREATER_THEN(">"), LESS_THEN("<"), EQUAL("="), GREATER_THEN_EQUAL(">="), LESS_THEN_EQUAL("<=");

    private String sign;

    Condition(String sign) {
        this.sign = sign;
    }

    public String getSign() {
        return sign;
    }

    public static Condition getCondition(String sign) {

        Optional<Condition> condition = Arrays.asList(Condition.values()).stream()
                .filter(conditions -> conditions.getSign().equals(sign))
                .findFirst();

        if (condition.isPresent()) {
            return condition.get();

        } else {
            throw new ValidationException("Wrong condition!");
        }


    }

}
