package com.euedge.coolstuff.domain.filter;

/**
 * It contains details for the classes which extends  {@link AbstractFieldSpecificFilter}. Using this object makes possible to define rules for filtering Product .
 */
public class Criteria {

    private String field;
    private Condition condition = Condition.EQUAL;
    private String value;
    private Boolean empty = Boolean.FALSE;


    public Criteria(String field, Condition condition, String value) {
        this.field = field;
        this.condition = condition;
        this.value = value;
    }

    public Criteria(String field, String condition, String value) {
        this.field = field;
        this.condition = Condition.getCondition(condition);
        this.value = value;
    }

    public Criteria() {
    }

    public Boolean getEmpty() {
        return empty;
    }

    public void setEmpty(Boolean empty) {
        this.empty = empty;
    }

    public String getField() {
        return field;
    }

    public Condition getCondition() {
        return condition;
    }

    public String getValue() {
        return value;
    }

    public void setField(String field) {
        this.field = field;
    }

    public void setCondition(String condition) {
        this.condition = Condition.getCondition(condition);
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Criteria{" +
                "field='" + field + '\'' +
                ", condition=" + condition +
                ", value='" + value + '\'' +
                ", empty=" + empty +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Criteria criteria = (Criteria) o;

        if (getField() != null ? !getField().equals(criteria.getField()) : criteria.getField() != null) return false;
        if (getCondition() != criteria.getCondition()) return false;
        return getValue() != null ? getValue().equals(criteria.getValue()) : criteria.getValue() == null;

    }

    @Override
    public int hashCode() {
        int result = getField() != null ? getField().hashCode() : 0;
        result = 31 * result + getCondition().hashCode();
        result = 31 * result + (getValue() != null ? getValue().hashCode() : 0);
        return result;
    }
}
