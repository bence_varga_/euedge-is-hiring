package com.euedge.coolstuff.domain.filter;

import com.euedge.coolstuff.domain.Product;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonTypeInfo(use = Id.NAME, property = "filter")
@JsonSubTypes({ @Type(name = "dummy", value = DummyFilter.class),
		@Type(name = "simpleFilter", value = SimpleFieldFilter.class),
		@Type(name = "tagsFilter", value = TagFieldFilter.class),
		@Type(name = "scoresFilter", value = ScoresFieldFilter.class)})
public interface ProductFilter {
	boolean apply(Product product);
}
