package com.euedge.coolstuff.domain.filter;

import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.ProductScore;

import java.util.Iterator;

/**
 * This class implements logic for the {@link com.euedge.coolstuff.domain.ProductScore} type field .
 */
public class ScoresFieldFilter extends AbstractFieldSpecificFilter {

    @Override
    protected boolean isProductFilteredByCriteria(Product product, Criteria criteria) {

        boolean result = false;
        if ((null == product.getScores() || product.getScores().isEmpty()) && !criteria.getEmpty()) return true;

        Iterator<ProductScore> iterator = product.getScores().iterator();

        while (iterator.hasNext() && !result) {
            ProductScore score = iterator.next();
            if ("score".equals(criteria.getField())) {
                result = evaluateNumber(score.getScore(), criteria);
            } else if ("comment".equals(criteria.getField())) {
                result = evaluateString(score.getComment(), criteria);
            }
        }
        return result;
    }
}
