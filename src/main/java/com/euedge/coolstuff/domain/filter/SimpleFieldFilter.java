package com.euedge.coolstuff.domain.filter;

import com.euedge.coolstuff.domain.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

/**
 * Implement a general filter logic. This can be used for all of the fields which are wrapped primitive Objects .
 * Supported objects: <br/>
 * <ul>
 * <li>String</li>
 * <li>Integer</li>
 * <li>Double</li>
 * <li>Long</li>
 * <ul/>
 */
public class SimpleFieldFilter extends AbstractFieldSpecificFilter {
    private static Logger logger = LoggerFactory.getLogger(SimpleFieldFilter.class);
    @Override
    protected boolean isProductFilteredByCriteria(Product product, Criteria criteria) {
        logger.debug("{}.isProductFilteredByCriteria has been called with parameters: {} , {}", new Object[]{this.getClass().getSimpleName(),product,criteria});
        try {

            Field field = product.getClass().getDeclaredField(criteria.getField());
            field.setAccessible(true);
            Object fieldValue = field.get(product);
            logger.debug("Field in the product has been found: {}. Its value is {}",new Object[]{criteria.getField(),fieldValue.toString()});

            switch (field.getType().getSimpleName()) {
                case "String":
                    return evaluateString(fieldValue, criteria);
                case "Integer":
                case "Double":
                case "Long":
                    return evaluateNumber((Double) fieldValue, criteria);
            }


        } catch (NoSuchFieldException | IllegalAccessException e) {
            logger.error("No given field. {}",e);
            throw new IllegalArgumentException("No field with name " + criteria.getField());
        }
        logger.debug("{}.isProductFilteredByCriteria is about to return with false.", new Object[]{this.getClass().getSimpleName()});
        return false;
    }


}
