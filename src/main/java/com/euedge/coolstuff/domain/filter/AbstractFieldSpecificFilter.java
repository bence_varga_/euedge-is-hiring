package com.euedge.coolstuff.domain.filter;

import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bence on 27/03/16 .
 */
public abstract class AbstractFieldSpecificFilter implements ProductFilter {
    private static Logger logger = LoggerFactory.getLogger(AbstractFieldSpecificFilter.class);
    private List<Criteria> rule = new ArrayList<>();

    @Override
    public boolean apply(Product product) {
        logger.debug("{}.apply has been called with parameter: {} ", new Object[]{this.getClass().getSimpleName(),product});
        boolean result = true;
        for (Criteria criteria : rule) {
            if (isProductFilteredByCriteria(product, criteria)){
                logger.debug("Product with {} name has not been filtered.",product.getName());
                return false;
            }
        }
        logger.debug("Product with {} name has not been filtered!",product.getName());
        return result;
    }


    public List<Criteria> getRule() {
        return rule;
    }

    public void setRule(List<Criteria> rule) {
        if (null != rule && !rule.isEmpty()) {
            this.rule = rule;
        }
    }

    protected boolean evaluateNumber(Double fieldValue, Criteria criteria) {
        logger.debug("{}.evaluateNumber has been called with parameter: {} , {} ", new Object[]{this.getClass().getSimpleName(),fieldValue,criteria});
        try {
            final int result = fieldValue.compareTo(Double.valueOf(criteria.getValue()));

            switch (criteria.getCondition()) {
                case GREATER_THEN:
                    return result > 0;
                case GREATER_THEN_EQUAL:
                    return result > 0 || result == 0;
                case LESS_THEN:
                    return result < 0;
                case LESS_THEN_EQUAL:
                    return result < 0 || result == 0;
                case EQUAL:
                default:
                    return result == 0;
            }
        } catch (NumberFormatException e) {
            throw new ValidationException("Provided value is not appropriate (" + criteria.getValue() + ") for the " + criteria.getField() + " field.");
        }
    }

    protected boolean evaluateString(Object fieldValue, Criteria criteria) {
        if (!criteria.getCondition().equals(Condition.EQUAL)) {
            throw new ValidationException("The " + criteria.getCondition().getSign() + " condition is not appropriate for the " + criteria.getField() + "field is not appropriate.");
        }

        return criteria.getValue().equals(fieldValue);
    }

    /**
     * It implements the logic whether the product is in the filter criteria or not.
     *
     * @param product  Product from the database
     * @param criteria A rule consist of multiple criteria which defines the filtering logic.
     * @return True if the product fulfill the rule. Otherwise false
     */
    protected abstract boolean isProductFilteredByCriteria(Product product, Criteria criteria);
}