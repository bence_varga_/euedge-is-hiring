package com.euedge.coolstuff.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.filter.ProductFilter;
import com.euedge.coolstuff.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	private ProductRepository repository;

	public List<Product> loadAllProducts() {
		return repository.findAll();
	}

	public List<Product> filterProducts(List<ProductFilter> filters) {
		List<Product> allProducts = loadAllProducts();

		List<Product> filterPassedProducts = new ArrayList<>();
		for (Product product : allProducts) {
			boolean filtered = false;
			// TODO: implement product field specific filters
			for (ProductFilter filter : filters) {
				if (!filter.apply(product)) {
					filtered = true;
					break;
				}
			}
			if (!filtered) {
				filterPassedProducts.add(product);
			}
		}
		return filterPassedProducts;
	}
}
