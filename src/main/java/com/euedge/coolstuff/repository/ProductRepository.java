package com.euedge.coolstuff.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.euedge.coolstuff.domain.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {

	@Override
	List<Product> findAll();


}
