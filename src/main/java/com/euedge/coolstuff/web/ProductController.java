package com.euedge.coolstuff.web;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.euedge.coolstuff.business.ProductService;
import com.euedge.coolstuff.domain.Product;
import com.euedge.coolstuff.domain.filter.ProductFilter;

@RestController
@RequestMapping("/api/product" )
public class ProductController {

    private static Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;

    @RequestMapping
    public List<Product> loadAllProducts() {
        return productService.loadAllProducts();
    }

    @RequestMapping(path = "filter", method = RequestMethod.POST)
    public List<Product> filterProducts(@RequestBody List<ProductFilter> filters) {
        return productService.filterProducts(filters);
    }

    // TODO: introduce paging and sorting(A-Z, Price, Popularity) functionality with filter

    // TODO: show available products first

    @RequestMapping(path = "search/{searchTerm}", method = RequestMethod.POST)
    public List<Product> search(@PathVariable String searchTerm) {
        // TODO: introduce search functionality
        return Collections.emptyList();
    }

    // TODO: introduce paging and sorting functionality with search

    // TODO: introduce multi-term search functionality

    // TODO: combine term search with filtering functionality

    // TODO: introduce error handling

}
